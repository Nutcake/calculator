import java.util.*;

public class calculator
{
  public static void main(String []args)
  {
    float a = 0;
    float b = 0;
    float aTemp = 0;
    float bTemp = 0;
    float result = 0;
    int opcount = 0;
    boolean err = false;
    List<Character> op = new ArrayList<Character>();
    List<String> nr = new ArrayList<String>();
    char temp;
    String expression;
    String delimiter = "[ +\\-*/]+";
    Scanner input = new Scanner(System.in);
    System.out.println("Input your mathematical expression. Enter q to quit.");

    while(true)
    {
      expression = input.nextLine();

      // Quit condition
      if (expression.charAt(0) == 'q')
      {
        return;
      }

      // Parsing
      String[] tokens = expression.split(delimiter);
      nr.addAll(Arrays.asList(tokens));
   

      for (int i = 0; i < expression.length(); i++)
      {
        temp = expression.charAt(i);
        if(temp == '+' || temp == '-' || temp == '*' || temp == '/')
        {
          op.add(temp);
          opcount++;
        }
      }

      System.out.println("Operators:\n"+op);
      System.out.println("Numbers, pre sort:\n"+nr);


      int index = 0;

      for (int x = 1; x < opcount; x++)
      {
        if (op.get(x) == '*')
        {
          op.remove(x);
          op.add(0, '*');
          aTemp = Float.parseFloat(nr.get(x));
          bTemp = Float.parseFloat(nr.get(x+1));
          nr.remove(x+1);
          nr.remove(x);
          nr.add(0, Float.toString(bTemp));
          nr.add(0, Float.toString(aTemp));
        }
        else if (op.get(x) == '/')
        {
          op.remove(x);
          op.add(0, '/');
          aTemp = Float.parseFloat(nr.get(x));
          bTemp = Float.parseFloat(nr.get(x+1));
          nr.remove(x+1);
          nr.remove(x);
          nr.add(0, Float.toString(bTemp));
          nr.add(0, Float.toString(aTemp));
        }
      }

      //tokens = nr.stream().toArray(String[]::new);

      System.out.println("Numbers, post sort:\n"+nr);
      System.out.println(op);

      try
      {
        a = Float.parseFloat(nr.get(0));
        b = Float.parseFloat(nr.get(1));
      } catch(NumberFormatException e1) {
        System.out.println("Input error, try again.");
        result = 0;
        err = true;
      }

      // Calculations
      for(int j = 1; j <= opcount; j++)
      {

        if (err == false)
        {
          switch(op.get(j-1)) 
          {
            case '+':	
              result = a + b;
            break;

            case '-':
              result = a - b;
            break;

            case '*':
              result = a * b;
            break;

            case '/':
              if (b != 0)
                 result = a / b;
              else
                System.out.println("Can't devide by zero");
            break;

          default: System.out.println("Unknown operator");
          }
        }
        System.out.format("%.2f %c %.2f = %.2f\n", a, op.get(j-1), b, result);
        a = result;
        try
        {
          b = Float.parseFloat(nr.get(j+1));
        } catch(IndexOutOfBoundsException e1) {}
      }
      err = false;
      op.clear();
      nr.clear();
      opcount = 0;
      System.out.println(result);
    }

  }

}
